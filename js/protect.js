//This is site.js
$(function () {


    var savedAge = localStorage.getItem("age");



    if (savedAge != null && savedAge != 'undefined' && parseInt(savedAge) >= 18) {

        $('#protectedContent').show()
    } else {

        $('#restrictedContent').show()
    }


    $('#btnAge').on("click", function () {

        var age = $('#age').val();

        localStorage.setItem("age", age);

        if (parseInt(age) >= 18) {
            $('#protectedContent').show()
            $('#restrictedContent').hide()
        }
    })


})